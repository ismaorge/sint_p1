# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.1] - 15/10/2018
### Changed
- Changed path for the image in the php file.
- Changed the validation of the email in the javascript.
- Changed the type of the input of email in the html from email(HTML5) to text.

## [1.0.0] - 08/10/2018
### Added
- Added the image of the background and the phpinfo.php file.
- Added alll the css needed.
### Changed
- Changed the charset of the html to UTF-8.
- Changed the table variables to a class "center" to center in the web page.
- Changed some td in the table with a class to paint in other color.
- Changed the locations of the hidden inputs in the html file. 

## [0.5.0] - 07/10/2018
### Added
- Added all the functions needed in php file.

## [0.4.0] - 01/10/2018
### Added
- Added a function to check all the checkboxes at the JavaScript.
### Changed
- Changed the structure of the html to have the fields of the requirements at this year (18-19).

## [0.3.0] - 24/09/2018
### Added
- Added function to show an image at php and comprobe the birthdate with javascript.

## [0.2.0] - 23/09/2018
### Added
- Added code to change de encoding type of the form.

## [0.1.0] - 23/09/2018
### Added
- Added code to read and write the hidden fields of time and browser version.

## [0.0.1] - 23/09/2018
### Added
- Added the code create so far.