<!DOCTYPE html>
<html lang="es">
  <head>
    <?php header('charset=utf-8'); ?>
    <link rel="stylesheet" href="p1.css" media="all"/>
    <title>PHP Test</title>
  </head>

  <body>
    <h3>Datos del usuario</h3>
    <table class="center">
      <tbody>
        <tr>
          <td class="point">id_usuario:</td>
          <td><?php echo $_REQUEST["user"] ?> </td>
        </tr>

        <tr>
          <td class="point">Contraseña:</td>
          <td><?php echo $_REQUEST["password"] ?></td>
        </tr>

        <tr>
          <td class="point">Nombre:</td>
          <td><?php echo $_REQUEST["nombre"] ?></td>
        </tr>
        
        <tr>
          <td class="point">Apellidos:</td>
          <td><?php echo $_REQUEST["fname"] ?></td>
        </tr>
          
        <tr>
          <td class="point">Fecha de nacimiento:</td>
          <td><?php echo $_REQUEST["birthdate"] ?></td>
        </tr>
        
        <tr>
          <td class="point">Género:</td>
          <td>
          <?php if(empty($_REQUEST["gender"])==false)
            {
              echo $_REQUEST["gender"];
            }
          ?>
          </td>
        </tr>
        
        <tr>
          <td class="point">Telf:</td>
          <td><?php echo $_REQUEST["tlf"] ?></td>
        </tr>
        
        <tr>
          <td class="point">E-mail:</td>
          <td><?php echo $_REQUEST["email"] ?></td>
        </tr>
      </tbody>
    </table>
    <h3>Álbum favorito</h3>
    <table class="center">
      <tbody>
        <tr>
          <td class="point">Portada:</td>
          <td> <?php 
          if(false==empty($_REQUEST['pic']))
          {
            echo $_REQUEST['pic'];
          }
          if(false==empty($_FILES))
          {
            $fichero=$_FILES["pic"]["name"];
            echo $fichero;
          }
                ?>
          </td>
        </tr> 
        
        <tr>
          <td class="point">Estilo:</td>
          <td><?php if(false==empty($_REQUEST["check"]))
            {
              for($i=0; $i<sizeof($_REQUEST["check"]);$i++)
              {
                if(empty(false==$_REQUEST["check"][$i]))
                {
                  echo $_REQUEST["check"][$i], "  ";
                }
              }
            }
          ?>
          </td>
        </tr>

        <tr>
          <td class="point">Época:</td>
          <td> <?php echo $_REQUEST["epoch"] ?></td>
        </tr> 

        <tr>
          <td class="point">Comentarios:</td>
          <td> <?php echo $_REQUEST["coment"] ?></td>
        </tr> 
      </tbody>
    </table>
    <h3>Método de envío:</h3>
    <table class="center">
      <tbody>
        <tr>
          <td class="point">URL de envío:</td>
          <td> <?php echo $_REQUEST["env"] ?></td>
        </tr> 

        <tr>
          <td class="point">Método de envío:</td>
          <td> <?php if($_SERVER['REQUEST_METHOD']=='POST')
                  {
                    echo "POST";
                  }
                  else
                  {
                    echo "GET";
                  }
           ?>
           </td>
        </tr> 

        <tr>
          <td class="point">Tipo de codificación:</td>
          <td> <?php if($_REQUEST['encoding']==1)
                  {
                    echo "application/x-www-form-urlencoded";
                  }
                  else
                  {
                    echo "multipart/form-data";
                  }
           ?>
           </td>
        </tr> 
      </tbody>
    </table>
    <br>
    <?php 
      //Show the image
     
      if(false==empty($_FILES))
      {
        $fichero=$_FILES["pic"]["name"];
        $source=$_FILES["pic"]["tmp_name"];
        $destination="/home/eetlabs.local/sint/sint40/public_html/p1". "/" .$fichero;

        if (true==move_uploaded_file($source,$destination))
        {
          echo "<img src=$fichero class='img'/>"; 
        }
      }
    ?>
    <br>
    <?php echo "Hora: ", $_REQUEST["hora"]?>
    <br>
    <?php echo "Navegador: ", $_REQUEST["navegador"] ?>
  </body>
</html>