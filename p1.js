function validar()
{
	//Comprobación del nombre
		name=document.getElementById("nombre").value;
		if (/^[A-Za-z\-\.\s\xF1\xD1\á,é,í,ó,ú,Á,É,Í,Ó,Ú]+$/.test(name)==false  && name != null && name.length != 0 ) 
		{
			alert('[ERROR] El NOMBRE introducido no es válido');
			return false;
		}

	//comprobación de los apellidos
		fsname=document.getElementById("fname").value;
		if (/^[A-Za-z\-\.\s\xF1\xD1\á,é,í,ó,ú,Á,É,Í,Ó,Ú]+$/.test(fsname)==false  && fsname != null && fsname.length != 0 ) 
		{
			alert('[ERROR] Los APELLIDOS introducidos no son válidos');
			return false;
		}

	//Comprobe hidden fields
		//Local time field

		document.getElementById("localTime").value=new Date().toLocaleTimeString();

		//User web
	
		document.getElementById("localBrowser").value=navigator.userAgent;


	//Comprobe the birthdate
		l_birthDate=document.getElementById("birthdate").value;
		if(!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(l_birthDate)&& l_birthDate != null && l_birthDate.length != 0 )
		{
			alert('[ERROR] La fecha de nacimiento es incorrecta.');
			return false;
		}else
		{
			var parts = l_birthDate.split("/");
			var day = parseInt(parts[0], 10);
			var month = parseInt(parts[1], 10);
			var year = parseInt(parts[2], 10);
		
			// Check the ranges of month, year and day
			if(year < 1900 || year > 2020 || month == 0 || month > 12 || day>31 || day<1)
			{
				alert('[ERROR] La fecha de nacimiento es incorrecta.');
				return false;
			}
				
		}
    
		
	//Comprobación del teléfono
		t=document.getElementById("tlf").value;
		if(!(/^\d{9}$/.test(t)) && t != null && t.length != 0)
		{
			alert('[ERROR] El TLF introducido no es válido');
			return false;
		}
					
	//comprobación email
		emai=document.getElementById("email").value;
		if( (/^\S+@\S+\.\S+$/.test(emai))==false && emai != null && emai.length != 0)    
		{
			alert('[ERROR] El EMAIL introducido no es válido');
			return false;
		}									

	//Comprobe the method to send
		metodo=document.getElementById("method").selectedIndex;

		switch(metodo)
		{
			case 0:
				alert('Método POST');
				formulario.method="POST";
				break;

			case 1:
				alert('Método GET');
				formulario.method="GET";
				break;

			default:
				alert('Método POST');
				formulario.method="POST";
				break;
		}
	//if(metodo == 1)
	// {
	// 	alert('Método GET');
	// 	formulario.method="GET";
	// }

	//Comprobe the encoding type
		encoding=document.getElementById("encoding").selectedIndex;
		switch(encoding)
		{
			case 0:
				formulario.enctype="application/x-www-form-urlencoded";
				break;

			case 1:
				formulario.enctype="multipart/form-data";
				break;

			default:
				formulario.enctype="application/x-www-form-urlencoded";
				break;
			
		}

	//Comprobe the URL
		url=document.getElementById("phpinfo");
		if(url.checked==true)
		{
			formulario.action="phpinfo.php";
		}else
		{
			formulario.action="p1.php";
		}
		return true;
}
function select_all(source)
{
 checkboxes=document.getElementsByName("check[]");
 check_length=checkboxes.length;
 for(var i=0; i<check_length; i++) 
 {
	checkboxes[i].checked = source.checked;
 }
}					
 